const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const arrTemp = []; // to store scores temporarily from user's input

class StudentSubjectScore{ // class for scores
    constructor(scores){
        this._scores = [...scores];
        this._highestScore = scores[scores.length-1];
        this._lowestScore = scores[0];
        this._average = ([...scores].reduce((prev,curr)=> prev += curr) / [...scores].length).toFixed(2);
        this._passed = [...scores].filter(score => score >= 75).length;
        this._notPassed = this._scores.length - this._passed;
    }
    lowestHighestScores(){
        console.log(`Highest Score : ${this._highestScore}`);
        console.log(`Lowest Score : ${this._lowestScore}`);
        console.log(`Student's subject scores in ascending order : `);
        console.log(...this._scores);
    }
    averageScore(){
        console.log(`Average Score : ${this._average}`);
    }
    passedOrNotPassed(){
        console.log(`the number of students who passed the exam : ${this._passed}`);
        console.log(`the number of students who didn't pass the exam : ${this._notPassed}`);
    }
}

const pauseProgram = (subjectScores) => { // function to pause and continue the program
    rl.question('press enter to continue. . .', () => chooseOutput(subjectScores));
}

const chooseOutput = (subjectScores) => { //a function to prompt user's desired output
    console.log(`
+------------------------------------------------------------------+
|                    Choose one of these options                   |
|------------------------------------------------------------------|
|1. input "1" to display the highest and lowest scores             |
|2. input "2" to display the average score                         |  
|3. input "3" to display the number of students who passed the exam|
|4. input "exit" to exit the program                               |
+------------------------------------------------------------------+`);
    rl.question('Choice : ', (choice) => {
        switch(choice){
            case '1' : subjectScores.lowestHighestScores(); pauseProgram(subjectScores);break;
            case '2' : subjectScores.averageScore();pauseProgram(subjectScores); break;
            case '3' : subjectScores.passedOrNotPassed(); pauseProgram(subjectScores);break;
            case 'exit' : console.log('Thank you for using this program!'); rl.close(); break;
            default : console.log('invalid choice!'); pauseProgram(subjectScores);break;
        }
    });
}

const scoreSort = () => { //a function to sort the scores in arrTemp using bubble sort
    let temp = 0;
    for(let i = 0; i < arrTemp.length; i++){
        for(let j = 0; j < arrTemp.length - i - 1; j++){
            if(arrTemp[j] > arrTemp[j+1]){
                temp = arrTemp[j+1];
                arrTemp[j+1] = arrTemp[j];
                arrTemp[j] = temp;
            }
        }
    }
}

//start the program
console.log(`
+------------------------------------------------+
|       Welcome to the score input Program       |
|------------------------------------------------|
|Input the scores and type "q" when you finished |
+------------------------------------------------+`);
rl.on('line', (input) => { //ask scores from user
    if(input === "q"){
        if(arrTemp.length > 0){
            scoreSort();
            const subjectScores = new StudentSubjectScore(arrTemp);
            chooseOutput(subjectScores);
        }else{
            console.log('please input the scores');
        }
    }
    else if(isNaN(input) || input === ''){
        console.log('Invalid input!,Please input number only');
    }else if(input < 0 || input > 100){
        console.log('Invalid score!, please input scores between 0 and 100');
    }
    else{
        arrTemp.push(parseFloat(input));
    }
});
